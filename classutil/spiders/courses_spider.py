import scrapy
from classutil.items import *

BASE_URL = "http://classutil.unsw.edu.au/"

class CoursesSpider( scrapy.Spider ):
	name = "courses"

	start_urls = [ "http://classutil.unsw.edu.au/" ]

	def parse( self, response ):
		for link in response.xpath( "//td[contains(@class,'data')]/a/@href" ):
			yield scrapy.Request( BASE_URL + link.extract( ), callback=self.parse_category )

	def parse_category( self, response ):
		table = response.xpath( "/html/body/table[3]/tr[position()>1]")
		item = Course( )
		item[ "sem" ] = response.url.replace( ".html", "" ).split( "/" )[ -1 ].split( "_" )[ -1 ]
		item[ "classes" ] = [ ]

		for row in table:
			row_class = row.css( "::attr(class)" ).extract_first( )
			if row_class in [ "rowHighlight", "rowLowlight" ]: 		# courses
				classItem = { }
				classItem[ "type" ] = row.xpath( "td[1]/text()" ).extract_first( ).strip( )
				classItem[ "status" ] = row.xpath( "td[5]/text()" ).extract_first( ).strip( )
				classItem[ "num_enr" ] = row.xpath( "td[6]/text()" ).extract_first( ).strip( )
				classItem[ "times" ] = row.xpath( "td[8]/text()" ).extract_first( ).strip( )

				item[ "classes" ].append( classItem )

			elif row_class == "cucourse": 							# course headers
				cCode = row.xpath( "td/b/text()" ).extract_first( ).strip( )
				cName = row.xpath( "td[2]/text()" ).extract_first( ).strip( )
				if "code" in item and item[ "code" ] != cCode:
					yield item
					item = Course( )
					item[ "classes" ] = [ ]
					item[ "sem" ] = response.url.replace( ".html", "" ).split( "/" )[ -1 ].split( "_" )[ -1 ]
				item[ "code" ] = cCode
				item[ "name" ] = cName
