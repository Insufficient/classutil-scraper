# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# put in items.py
class Course( scrapy.Item ):
	name = scrapy.Field( )
	code = scrapy.Field( )
	classes = scrapy.Field( )
	sem = scrapy.Field( )
